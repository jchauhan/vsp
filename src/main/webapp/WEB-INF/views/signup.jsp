<html>
<head>
<title>Spring 3.0 MVC Series: Hello World - ViralPatel.net</title>
<link href="<%=request.getContextPath()%>/public/css/bootstrap.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/public/css/bootstrap-responsive.css" rel="stylesheet">

<script src="<%=request.getContextPath()%>/public/js/jquery-1.8.2.min.js"></script>
<script src="<%=request.getContextPath()%>/public/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/public/js/bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function()
{
// Popover 
//$('#registerHere input').hover(function()
//{
//$(this).popover('show')
//});

// Validation
$("#registerHere").validate({
rules:{
name:"required",
username:{required:true,minlength: 3},
email:{required:true,email: true},
unencryptedPassword:{required:true,minlength: 6},
passwordConfirm:{required:true,equalTo: "#pwd"},
gender:"required"
},

messages:{
name:"Enter your first and last name",
username:{
	required:"Please choose your username",
	minlength:"Username must be minimum 3 characters"},
email:{
required:"Enter your email address",
email:"Enter valid email address"},
unencryptedPassword:{
required:"Enter your password",
minlength:"Password must be minimum 6 characters"},
passwordConfirm:{
required:"Enter confirm password",
equalTo:"Password and Confirm Password must match"},
gender:"Select Gender"
},

errorClass: "help-inline",
errorElement: "span",
highlight:function(element, errorClass, validClass)
{
$(element).parents('.control-group').addClass('error');
},
unhighlight: function(element, errorClass, validClass)
{
$(element).parents('.control-group').removeClass('error');
$(element).parents('.control-group').addClass('success');
}
});
});
</script>


</head>
  <body>
	<form class="form-horizontal" id="registerHere" method='post' action="<%=request.getContextPath()%>/app/user/create">
		<fieldset>
			<legend>Registration</legend>
			<div class="control-group">
				<label class="control-label" for="input01">Name</label>
				<div class="controls">
					<input type="text" class="input-xlarge" id="name"
						name="name" rel="popover"
						data-content="Enter your first and last name."
						data-original-title="Full Name">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="input01">Username</label>
				<div class="controls">
					<input type="text" class="input-xlarge" id="username"
						name="username" rel="popover"
						data-content="Enter Choose Your Username"
						data-original-title="Username">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="input01">Email</label>
				<div class="controls">
					<input type="text" class="input-xlarge" id="email"
						name="email" rel="popover"
						data-content="What’s your email address?"
						data-original-title="Email">

				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="input01">Password</label>
				<div class="controls">
					<input type="password" class="input-xlarge" id="pwd" name="unencryptedPassword"
						rel="popover" data-content="6 characters or more! Be tricky"
						data-original-title="Password">

				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="input01">Confirm Password</label>
				<div class="controls">
					<input type="password" class="input-xlarge" id="cpwd" name="passwordConfirm"
						rel="popover"
						data-content="Re-enter your password for confirmation."
						data-original-title="Re-Password">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="input01">Gender</label>
				<div class="controls">
					<select name="gender" id="gender">
						<option value="">Gender</option>
						<option value="male">Male</option>
						<option value="female">Female</option>
						<option value="other">Other</option>
					</select>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="input01"></label>
				<div class="controls">
					<button type="submit" class="btn btn-success" rel="tooltip"
						title="first tooltip">Signup</button>
					<a class="btn btn-primary" href="https://www.facebook.com/dialog/oauth?client_id=102273229879534&redirect_uri=http://localhost:8080/vcs/app/user/fbsignin">Signup with Facebook</a>
				</div>
			</div>
		</fieldset>
	</form>
  </body>
</html>

