<html>
<head>
<title>Spring 3.0 MVC Series: Hello World - ViralPatel.net</title>
<link href="<%=request.getContextPath()%>/public/css/bootstrap.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/public/css/bootstrap-responsive.css" rel="stylesheet">

<script src="<%=request.getContextPath()%>/public/js/jquery-1.8.2.min.js"></script>
<script src="<%=request.getContextPath()%>/public/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/public/js/bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function()
{
// Popover 
//$('#loginHere input').hover(function()
//{
//$(this).popover('show')
//});

// Validation
$("#loginHere").validate({
rules:{
username:{required:true,minlength: 3},
unencryptedPassword:{required:true,minlength: 6},
},

messages:{
username:{
	required:"Please choose your username",
	minlength:"Username must be minimum 3 characters"},
unencryptedPassword:{
required:"Enter your password",
minlength:"Password must be minimum 6 characters"},
},

errorClass: "help-inline",
errorElement: "span",
highlight:function(element, errorClass, validClass)
{
$(element).parents('.control-group').addClass('error');
},
unhighlight: function(element, errorClass, validClass)
{
$(element).parents('.control-group').removeClass('error');
$(element).parents('.control-group').addClass('success');
}
});
});
</script>


</head>
  <body>
	<form class="form-horizontal" id="loginHere" method='post' action="<%=request.getContextPath()%>/app/user/login.html">
		<fieldset>
			<legend>Login</legend>
			<div class="control-group">
				<label class="control-label" for="input01">Username</label>
				<div class="controls">
					<input type="text" class="input-xlarge" id="username"
						name="username" rel="popover"
						data-content="Enter Choose Your Username"
						data-original-title="Username">
				</div>
			</div>


			<div class="control-group">
				<label class="control-label" for="input01">Password</label>
				<div class="controls">
					<input type="password" class="input-xlarge" id="pwd" name="unencryptedPassword"
						rel="popover" data-content="6 characters or more! Be tricky"
						data-original-title="Password">

				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="input01"></label>
				<div class="controls">
					<button type="submit" class="btn btn-success" rel="tooltip"
						title="first tooltip">Sigin</button>
					<a class="btn btn-primary" href="https://www.facebook.com/dialog/oauth?client_id=102273229879534&redirect_uri=http://localhost:8080/vcs/app/user/fbsignin">Signin with Facebook</a>
				</div>
			</div>
		</fieldset>
	</form>
  </body>
</html>

