package com.myvouchee.vcs.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.IndexColumn;
import org.hibernate.validator.constraints.Email;

import com.myvouchee.commons.domain.AuditableEntity;
import com.myvouchee.commons.domain.Constants;

@Entity
@Table(name = "table_fb_users"
)
public class FbUser extends AuditableEntity 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8147716104126487358L;
	
	Long userId;
	Long fbUserId;
	String name;
	String firstName;
	String lastName;
	String email;	
	String username;
	Date birthday;
	String gender;
	
	@Column(nullable = false, unique=true)
	@IndexColumn(name="FbUserUserId")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(nullable = false, unique=true)
	@IndexColumn(name="FbUserFbUserId")
	public Long getFbUserId() {
		return fbUserId;
	}
	public void setFbUserId(Long fbUserId) {
		this.fbUserId = fbUserId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(length = Constants.FB_EMAIL_LENGTH, nullable = false, unique=true)
    @Email
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(length = Constants.FB_USERNAME_LENGTH, nullable = false, unique=true)
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
}
