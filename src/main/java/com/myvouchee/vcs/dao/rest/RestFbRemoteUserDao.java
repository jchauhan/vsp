package com.myvouchee.vcs.dao.rest;

import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.myvouchee.commons.utils.Utils;
import com.myvouchee.vcs.dao.FbRemoteUserDao;
import com.myvouchee.vcs.domain.FbUser;
import com.myvouchee.vcs.web.app.UserController;

/**
 * REST API interface of Remote Facebook APIs
 * 
 * @author jitendra chauhan
 */
@Repository
public class RestFbRemoteUserDao implements FbRemoteUserDao{
	private static final Logger log = LoggerFactory.getLogger(UserController.class);

	@Override
	public FbUser retrieveByAccessCode(String accessCode) throws Exception {
		URL url = new URL("https://graph.facebook.com/" + accessCode);
		String response = Utils.fetchURLAsString(url);
		log.debug(response);
		System.out.println(response);
		FbUser fbUser = parseFbUserResponse(response);
		return fbUser;
	}
	
	@Override
	public FbUser retrieveById(long id) throws Exception {
		URL url = new URL("https://graph.facebook.com/" + id);
		String response = Utils.fetchURLAsString(url);
		log.debug(response);
		System.out.println(response);
		FbUser fbUser = parseFbUserResponse(response);
		return fbUser;
	}

	@Override
	public FbUser retrieveByUsername(String username) throws Exception{
		URL url = new URL("https://graph.facebook.com/" + username);
		String response = Utils.fetchURLAsString(url);
		FbUser fbUser = parseFbUserResponse(response);
		return fbUser;
	}
	
	@Override
	public FbUser parseFbUserResponse(String fbResponse) throws RuntimeException
	{
		JSONObject respjson;
		FbUser fbUser = new FbUser();
		try {
			respjson = new JSONObject(fbResponse);
			fbUser.setFbUserId(Long.parseLong(respjson.getString("id")));
			fbUser.setFirstName(respjson.has("first_name") ? respjson.getString("first_name"):"");
			fbUser.setLastName(respjson.has("last_name") ? respjson.getString("last_name") : "");
			fbUser.setEmail(respjson.has("email") ? respjson.getString("email") : null);
			fbUser.setUsername(respjson.has("username") ? respjson.getString("username") : null);
			fbUser.setName(respjson.has("name") ? respjson.getString("name") : null);
		} catch (JSONException e) {
			
			throw new RuntimeException(e);
		}
		return fbUser;
	}
}
