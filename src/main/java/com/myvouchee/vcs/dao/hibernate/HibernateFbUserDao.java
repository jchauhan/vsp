/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012 ..
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.vcs.dao.hibernate;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.myvouchee.vcs.dao.FbUserDao;
import com.myvouchee.vcs.domain.FbUser;



/**
 * Hibernate FbUser DAO implementation. 
 * 
 * @author Jitendra Chahan
 */
@Repository
public class HibernateFbUserDao implements FbUserDao {

	private SessionFactory sessionFactory;

	@Autowired
	public HibernateFbUserDao(@Qualifier("vcs") SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<FbUser> retrieveAll() {
		return sessionFactory.getCurrentSession().createQuery("from FbUser user order by user.name")
				.list();
	}

	@Override
	public FbUser retrieveById(long id) {
		return (FbUser) sessionFactory.getCurrentSession().get(FbUser.class, id);
	}

	@Override
	public FbUser retrieveByUsername(String name) {
		return (FbUser) sessionFactory.getCurrentSession()
				.createQuery("from FbUser user where user.name = :name").setString("name", name)
				.uniqueResult();
	}

	@Override
	public void saveOrUpdate(FbUser user) {
		if (user.getId() != null) {
			sessionFactory.getCurrentSession().merge(user);
		} else {
			sessionFactory.getCurrentSession().saveOrUpdate(user);
		}
	}

	@Override
	public void deleteById(long id) {
		sessionFactory.getCurrentSession().delete(retrieveById(id));
	}

	@Override
	public FbUser retrieveByFbUserId(Long fbUserId) {
		return (FbUser) sessionFactory.getCurrentSession()
				.createQuery("from FbUser user where user.fbUserId = :fbUserId").setLong("fbUserId", fbUserId)
				.uniqueResult();
	}
	
	@Override
	public FbUser retrieveByUserId(Long userId) {
		return (FbUser) sessionFactory.getCurrentSession()
				.createQuery("from FbUser user where user.userId = :userId").setLong("userId", userId)
				.uniqueResult();
	}
}
