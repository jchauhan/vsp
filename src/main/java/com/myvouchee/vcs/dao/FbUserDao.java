/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.vcs.dao;

import java.util.List;

import com.myvouchee.vcs.domain.FbUser;



/**
 * Basic DAO class for the FbUser entity.
 * 
 * @author Jitendra Chauhan
 */
public interface FbUserDao {

	/**
	 * @return
	 */
	List<FbUser> retrieveAll();

	/**
	 * @param id
	 * @return
	 */
	FbUser retrieveById(long id);

	/**
	 * @param username
	 * @return
	 */
	FbUser retrieveByUsername(String username);
	

	/**
	 * @param fbUserId
	 * @return
	 */
	FbUser retrieveByFbUserId(Long fbUserId);
	
	/**
	 * @param userId
	 * @return
	 */
	FbUser retrieveByUserId(Long userId);

	/**
	 * @param user
	 */
	void saveOrUpdate(FbUser user);

	/**
	 * @param id
	 */
	void deleteById(long id);
}
