package com.myvouchee.vcs.dao;

import com.myvouchee.vcs.domain.FbUser;

public interface FbRemoteUserDao 
{
	/**
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	FbUser retrieveById(long id) throws Exception;

	/**
	 * @param username
	 * @return
	 */
	FbUser retrieveByUsername(String username) throws Exception;

	
	/**
	 * @param accessCode
	 * @return
	 */
	FbUser retrieveByAccessCode(String accessCode) throws Exception;

	FbUser parseFbUserResponse(String fbResponse) throws RuntimeException;
}
