/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012 
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.vcs.web.validator;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Projections.count;
import static org.hibernate.criterion.Restrictions.eq;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

public class UniqueConstraintValidator implements
		ConstraintValidator<Unique, String> {

	@Autowired
	private SessionFactory sessionFactory;

	private Class<?> entity;
	private String field;

	@Override
	public void initialize(Unique annotation) {
		this.entity = annotation.entity();
		this.field = annotation.field();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		// Not a good determination, why sessionFactory is null if
		// the username is valid?
		if (sessionFactory == null) {
			return true;
		}
		if (StringUtils.isEmpty(value)) {
			return false;
		}
		return query(value).intValue() == 0;
	}

	private Number query(String value) {
		HibernateTemplate template = new HibernateTemplate(sessionFactory);
		DetachedCriteria criteria = forClass(entity).add(eq(field, value))
				.setProjection(count(field));
		return (Number) template.findByCriteria(criteria).iterator().next();
	}

}
