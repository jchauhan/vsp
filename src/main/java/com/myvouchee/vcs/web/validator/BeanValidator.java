/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012 
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.vcs.web.validator;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component("BeanValidator")
public class BeanValidator implements org.springframework.validation.Validator, InitializingBean {

	private Validator validator;

	@Override
	public boolean supports(@SuppressWarnings("rawtypes") Class clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (validator == null) {
			ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
			validator = validatorFactory.usingContext().getValidator();
		}

		Set<ConstraintViolation<Object>> constraintViolations = validator.validate(target);
		for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
			String propertyPath = constraintViolation.getPropertyPath().toString();
			String message = constraintViolation.getMessage();

			errors.rejectValue(propertyPath, "", message);
		}
	}

	@Override
	public void afterPropertiesSet() {
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.usingContext().getValidator();
	}

}
