/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.vcs.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.myvouchee.sp.domain.User;

public class UserValidator implements Validator {

	@Override
	public boolean supports(@SuppressWarnings("rawtypes") Class clazz) {
		return User.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		User user = (User) target;

		if (isEmptyOrWhitespace(user.getName()))
			errors.rejectValue("name", "errors.required", new String[] { "Name" }, null);
		else if (user.getName() != null && user.getName().length() > 25) {
			errors.rejectValue("name", null, "Name has a maximum length of 25.");
		}

		// Validate role
		if ((user.getRole() == null) || (user.getRole().getId() == null)
				|| (user.getRole().getId() == 0)) {
			errors.rejectValue("role.id", "errors.required", new String[] { "Role" }, null);
		}

		// Validate password
		if (user.isNew()) {
			if (isEmptyOrWhitespace(user.getUnencryptedPassword())) {
				errors.rejectValue("password", "errors.required", new String[] { "Password" }, "");
			}
		}

		if (user.getUnencryptedPassword() != null && user.getUnencryptedPassword().length() > 25) {
			errors.rejectValue("password", null, "Password has a maximum length of 25.");
		}

		// Confirm password
		if (errors.getFieldError("password") == null) {
			if (!isEmptyOrWhitespace(user.getUnencryptedPassword())
					|| !isEmptyOrWhitespace(user.getPasswordConfirm())) {
				if (isEmptyOrWhitespace(user.getUnencryptedPassword())) {
					errors.rejectValue("password", null, "Passwords do not match.");
				} else if (isEmptyOrWhitespace(user.getPasswordConfirm())) {
					errors.rejectValue("password", null, "Passwords do not match.");
				} else if (!user.getUnencryptedPassword().equals(user.getPasswordConfirm())) {
					errors.rejectValue("password", null, "Passwords do not match.");
				}
			}
		}
	}

	private boolean isEmptyOrWhitespace(String value) {
		return (value == null) || (value.trim().equals(""));
	}
}
