package com.myvouchee.vcs.web.app;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.myvouchee.commons.web.filter.realm.FacebookToken;
import com.myvouchee.sp.domain.FacebookUserDetails;
import com.myvouchee.sp.domain.User;
import com.myvouchee.sp.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	private static final Logger log = LoggerFactory.getLogger(UserController.class);
	UserService userService;

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public ModelAndView showRegisterUserPage() {

		String message = "Hello World, Spring 3.0!";
		return new ModelAndView("signup", "message", message);
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView showUserLoginPage() {
		Subject currentUser = SecurityUtils.getSubject();
		if(currentUser.isAuthenticated())
		{
			String message = "Welcome User!!!";
			return new ModelAndView("dashboard", "message", message);
		}
		else
			return new ModelAndView("login", "message", "Please Login");
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView failureLoginPage(@ModelAttribute User user) 
	{
		Subject currentUser = SecurityUtils.getSubject();
		if(currentUser.isAuthenticated())
		{
			String message = "Welcome User!!!";
			return new ModelAndView("dashboard", "message", message);
		}
		else
			return new ModelAndView("login", "userCredentials", user);
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout() {
		Subject currentUser = SecurityUtils.getSubject();
		if(currentUser.isAuthenticated())
		{
			currentUser.logout();
			return new ModelAndView("login", "message", "Successfully Logged Out!!");
		}
		else
			return new ModelAndView("login", "message", "Please Login");
	}


	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ModelAndView createUser(@ModelAttribute User user) {
		userService.createUser(user);
		String message = "Successfully Registered";
		return new ModelAndView("register", "message", message);
	}

	@RequestMapping(value = "/fbsignin", method = RequestMethod.GET)
	public ModelAndView fbsignin(HttpServletRequest request, HttpServletResponse response) {
		Subject currentUser = SecurityUtils.getSubject();
		if(!currentUser.isAuthenticated())
		{
			String code = request.getParameter("code");
			FacebookToken facebookToken = new FacebookToken(code);
			try{
				currentUser.login(facebookToken);
				String fbUserId = (String)currentUser.getPrincipals().asList().get(0);
				FacebookUserDetails facebookUserDetails = (FacebookUserDetails)currentUser.getPrincipals().asList().get(2);
				log.debug("Principal " + currentUser.getPrincipal());
				log.debug("Facebook Id " + fbUserId);
				userService.storeOrUpdateFbUser(Long.parseLong(fbUserId), facebookUserDetails.getJsonString());
			}
			catch(Exception ae)
			{
				log.debug("Exception while authentication", ae);
				return new ModelAndView("login", "message", "Login Failure");
			}
		}
		
		log.debug("Authentication sucessfull...");
		String message = "Welcome User!!!";
		return new ModelAndView("dashboard", "message", message);
	}	

	@RequestMapping(value = "/fbsignup", method = RequestMethod.GET)
	public ModelAndView fbsignup(HttpServletRequest request, HttpServletResponse response) {
		return fbsignin(request, response);
	}	

}
