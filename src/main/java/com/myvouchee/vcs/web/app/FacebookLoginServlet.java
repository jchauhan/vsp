package com.myvouchee.vcs.web.app;

import java.io.IOException;

import javax.security.sasl.AuthenticationException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.SecurityUtils;

import com.myvouchee.commons.web.filter.realm.FacebookToken;


/**
 * Simple Facebook Login Handling, doesn't actually do anything except display page confirming login
 * successfull.
 *
 *
 * @author Jitendra Chauhan
 *
 */

/*
 * We are using spring controllers to handler the events 
 */
@Deprecated
public class FacebookLoginServlet  extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = request.getParameter("code");
		FacebookToken facebookToken = new FacebookToken(code);
		try{
			SecurityUtils.getSubject().login(facebookToken);
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/app/home/dashboard.html"));
		}
		catch(AuthenticationException ae){
			throw new ServletException(ae);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	IOException {
		System.out.println("Unexpected doPost ...");
	}
}
