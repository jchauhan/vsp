package com.myvouchee.vcs.web.app;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/home")
public class HomeController 
{
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)	
    public ModelAndView showDashboard() { 
        String message = "Welcome User!!!";
        return new ModelAndView("dashboard", "message", message);
    }
}
