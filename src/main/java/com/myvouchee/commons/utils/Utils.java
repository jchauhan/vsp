package com.myvouchee.commons.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class Utils {
	
	public static String fetchURLAsString(URL url) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = url.openStream();
		int r;
		while ((r = is.read()) != -1) {
			baos.write(r);
		}
		return new String(baos.toByteArray());
	}

	public static Map<String, String> getPropsMap(String someString) {
		String[] pairs = someString.split("&");
		Map<String, String> props = new HashMap<String, String>();
		for (String propPair : pairs) {
			String[] pair = propPair.split("=");
			props.put(pair[0], pair[1]);
		}
		return props;
	}
}
