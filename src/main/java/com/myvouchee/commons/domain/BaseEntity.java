/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012 ...
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.commons.domain;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * A base class for all entities that ensures Serialization and PK generation.
 * 
 * A primer on annotations is below. More information can be obtained from the
 * Hibernate docs here:
 * 
 * http://docs.jboss.org/hibernate/stable/annotations/reference/en/html_single/
 * 
 * @Column( name="columnName"; (1) boolean unique() default false; (2) boolean
 *          nullable() default true; (3) boolean insertable() default true; (4)
 *          boolean updatable() default true; (5) String columnDefinition()
 *          default ""; (6) String table() default ""; (7) int length() default
 *          255; (8) int precision() default 0; // decimal precision (9) int
 *          scale() default 0; // decimal scale
 * 
 * @author Jitendra Chauhan
 * 
 */
@MappedSuperclass
public class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id = null;
	
	public BaseEntity() {
		super();
	}

	public BaseEntity(Long id) {
		super();
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Transient
	@JsonIgnore
	public boolean isNew() {
		return this.getId() == null;
	}
}
