/*******************************************************************************
 * 
 * Copyright (c) 2006-2012 ....
 ******************************************************************************/
package com.myvouchee.commons.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * A base class for all entities that require Created and Modified dates.
 * @author Jitendra Chuahan
 */
@MappedSuperclass
public class AuditableEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private Date createdDate = new Date();
	private Date modifiedDate = new Date();
	//UserId of the user who has created the entry
	private Long createdBy = 0L;
	//UserIf of the user who has modified the entry
	private Long modifiedBy = 0L;

	private boolean active = true;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	@JsonIgnore
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Column(nullable = false)
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Column(nullable = false)
	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	@Column(nullable = false)
	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
}
