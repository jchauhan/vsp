package com.myvouchee.commons.domain;

public class Constants {

	public static final int NAME_LENGTH = 80;
	public static final int PASSWORD_LENGTH = 256;
	public static final int GENDER_LENGTH = 80;
	public static final int EMAIL_LENGTH = 156;	
	public static final int USERNAME_LENGTH = 156;		
	public static final int LOCALE_LENGTH = 156;
	public static final int MOBILE_LENGTH = 156;
	
	
	public static final int FB_FIRSTNAME_LENGTH = 80;
	public static final int FB_LASTNAME_LENGTH = 80;
	public static final int FB_MIDDLE_LENGTH = 80;
	public static final int FB_NAME_LENGTH = NAME_LENGTH;
	public static final int FB_USERNAME_LENGTH = USERNAME_LENGTH;
	public static final int FB_EMAIL_LENGTH = EMAIL_LENGTH;	
	
}
