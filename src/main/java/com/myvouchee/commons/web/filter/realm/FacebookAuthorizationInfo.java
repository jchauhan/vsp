package com.myvouchee.commons.web.filter.realm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;

public class FacebookAuthorizationInfo implements AuthorizationInfo
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2071485528261731804L;

	@Override
	public Collection<Permission> getObjectPermissions() {
		return new ArrayList<Permission>();
	}

	@Override
	public Collection<String> getRoles() {
		List<String> roles = new ArrayList<String>();
		roles.add("END_USER");
		return roles;
	}

	@Override
	public Collection<String> getStringPermissions() 
	{
		return new ArrayList<String>();
	}
}
