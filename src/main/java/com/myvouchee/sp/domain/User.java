/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012 ..
 * ////////////////////////////////////////////////////////////////////////
 * @author Jitendra Chauhan
 ******************************************************************************/
package com.myvouchee.sp.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.IndexColumn;
import org.hibernate.validator.constraints.Email;
import com.myvouchee.commons.domain.AuditableEntity;

@Entity
@Table(name = "table_users"
)
public class User extends AuditableEntity {

	private static final long serialVersionUID = -5821877436246475858L;
	
	public static final int NAME_LENGTH = 80;
	public static final int PASSWORD_LENGTH = 256;
	public static final int GENDER_LENGTH = 80;
	public static final int EMAIL_LENGTH = 156;	
	public static final int USERNAME_LENGTH = 156;		
	public static final int LOCALE_LENGTH = 156;
	public static final int MOBILE_LENGTH = 156;

	private String name;
	
	private String username;
	private String gender;
	private String email;
	private String mobile = "";
	
	private float timezone = 5.5f;
	private String locale = "en-US";
	private boolean verified = false;
	
	private String password;
	private String salt;
	private boolean approved = true;
	private boolean locked = false;
	private Date lastLoginDate = new Date();
	private Date lastPasswordChangedDate = new Date();
	private int failedPasswordAttempts = 0;
	private Date failedPasswordAttemptWindowStart = new Date();

	private Role role;

	private String unencryptedPassword;
	private String passwordConfirm;

	@Column(length = NAME_LENGTH, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(length = GENDER_LENGTH)
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Column(length = USERNAME_LENGTH, nullable = false, unique=true)
	@IndexColumn(name="UserUserId")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(length = EMAIL_LENGTH, nullable = false, unique=true)
	@IndexColumn(name="UserEmail")
    @Email
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getMobile() {
		return mobile;
	}

	@Column(length = MOBILE_LENGTH)
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(nullable = false)
	public float getTimezone() {
		return timezone;
	}

	public void setTimezone(float timezone) {
		this.timezone = timezone;
	}

	@Column(length = LOCALE_LENGTH, nullable = false)
	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	@Column(nullable = false)
	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	@Column(length = PASSWORD_LENGTH, nullable = false)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(length = PASSWORD_LENGTH, nullable = false)
	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	@Column(nullable = false)
	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	@Column(nullable = false)
	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getLastPasswordChangedDate() {
		return lastPasswordChangedDate;
	}

	public void setLastPasswordChangedDate(Date lastPasswordChangedDate) {
		this.lastPasswordChangedDate = lastPasswordChangedDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	@Column(nullable = false)
	public int getFailedPasswordAttempts() {
		return failedPasswordAttempts;
	}

	public void setFailedPasswordAttempts(int failedPasswordAttempts) {
		this.failedPasswordAttempts = failedPasswordAttempts;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getFailedPasswordAttemptWindowStart() {
		return failedPasswordAttemptWindowStart;
	}

	public void setFailedPasswordAttemptWindowStart(Date failedPasswordAttemptWindowStart) {
		this.failedPasswordAttemptWindowStart = failedPasswordAttemptWindowStart;
	}

	@ManyToOne
	@JoinColumn(name = "roleId", nullable = false)
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Transient
	public String getUnencryptedPassword() {
		return unencryptedPassword;
	}

	public void setUnencryptedPassword(String unencryptedPassword) {
		this.unencryptedPassword = unencryptedPassword;
	}

	@Transient
	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}
}
