package com.myvouchee.sp.domain;

import java.util.Properties;

public class FacebookProperties
{
	public Properties getProperties()
	{
		Properties props = new Properties();
		
		props.put("fbAppSecret", "11ff0821a852b02f01f04ce0a2761e07");
		props.put("fbAppId", "102273229879534");
		props.put("fbLoginRedirectURL", "http://localhost:8080/vcs/app/user/fbsignin");
		return props;
	}
}