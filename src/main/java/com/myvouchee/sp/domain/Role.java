/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012 ..
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.sp.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

import com.myvouchee.commons.domain.AuditableEntity;

/*
 * A class that define a role
 * @author Jitendra Chauhan
 */

@Entity
@Table(name = "table_user_roles")
public class Role extends AuditableEntity {

	private static final long serialVersionUID = -1609499610449048270L;

	@NotEmpty(message = "{errors.required}")
	@Size(max = 80, message = "{errors.maxlength}")
	private String name;
	
	@NotEmpty(message = "{errors.required}")
	@Size(max = 80, message = "{errors.maxlength}")
	private String displayName;

	private List<User> users;

	@Column(length = 80, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(length = 80, nullable = false)
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@OneToMany(mappedBy = "role")
	@JsonIgnore
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
}
