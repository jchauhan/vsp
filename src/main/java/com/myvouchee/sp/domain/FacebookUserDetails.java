package com.myvouchee.sp.domain;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Simple class for holding data relating to a facebook user
 *
 * @author Jitendra Chauhan
 *
 */
public class FacebookUserDetails {
	private String id;
	private String firstName;
	private String lastName;
	private String email;
	private String accessToken;
	// jsonString Expected to be something like this
	// {
	// "education": [{
	// "school": {
	// "id": "123456789012345",
	// "name": "University of Sheffield"
	// },
	// "type": "Graduate School",
	// "with": [{
	// "id": "123456789",
	// "name": "Daffy Duck"
	// }]
	// }],
	// "first_name": "Mike",
	// "id": "121212121",
	// "last_name": "Warren",
	// "link":
	// "http://www.facebook.com/profile.php?id=121212121",
	// "locale": "en_US",
	// "name": "Mike Warren",
	// "updated_time": "2011-08-15T14:51:05+0000",
	// "verified": true
	// }
	
//	{"id":"591117138",
//	"name":"Jitendra S Chauhan",
//	"first_name":"Jitendra",
//	"middle_name":"S",
//	"last_name":"Chauhan",
//	"link":"http:\/\/www.facebook.com\/jitendra.s.chauhan",
//	"username":"jitendra.s.chauhan",
//	"birthday":"11\/06\/1981",
//	"hometown":{"id":"108758412482112","name":"Gwalior"},
//	"location":{"id":"106377336067638","name":"Bangalore, India"},
//	"education":[{"school":{"id":"322199687875554","name":"IIT Kharagpur"},
//		"year":{"id":"138383069535219","name":"2005"},
//		"concentration":[{"id":"162557380459569","name":"M.Tech"}],
//		"type":"College"}],
//	"gender":"male",
//	"relationship_status":"Married",
//	"significant_other":{"name":"Manisha Chauhan","id":"100000706826633"},
//	"email":"jitendra.chauhan\u0040gmail.com",
//	"timezone":5.5,
//	"locale":"en_US",
//	"verified":true,
//	"updated_time":"2012-10-19T10:43:43+0000"}

	
	private String jsonString;

	public FacebookUserDetails(String fbResponse){
		jsonString = fbResponse;
		JSONObject respjson;
		try {
			respjson = new JSONObject(fbResponse);
			this.id = respjson.getString("id");
			this.firstName = respjson.has("first_name") ? respjson.getString("first_name") : "" + id;
			this.lastName = respjson.has("last_name") ? respjson.getString("last_name") : "";
			this.email = respjson.has("email") ? respjson.getString("email") : "";
		} catch (JSONException e) {
			System.out.println( "fbResponse:"+fbResponse );
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public String toString(){
		return jsonString;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
}
