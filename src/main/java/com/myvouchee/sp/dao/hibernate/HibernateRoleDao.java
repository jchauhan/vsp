/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012 ...
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.sp.dao.hibernate;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.myvouchee.sp.dao.RoleDao;
import com.myvouchee.sp.domain.Role;


/**
 * Hibernate Role DAO implementation. Most basic methods are implemented in the
 * AbstractGenericDao
 * 
 * @author jitendra chauhan
 */
@Repository
public class HibernateRoleDao implements RoleDao {

	private SessionFactory sessionFactory;

	@Autowired
	public HibernateRoleDao(@Qualifier("sp") SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Role> retrieveAll() {
		return sessionFactory.getCurrentSession().createQuery("from Role role order by role.name")
				.list();
	}

	@Override
	public Role retrieveById(long id) {
		return (Role) sessionFactory.getCurrentSession().get(Role.class, id);
	}

	@Override
	public Role retrieveByName(String name) {
		return (Role) sessionFactory.getCurrentSession()
				.createQuery("from Role role where role.name = :name").setString("name", name)
				.uniqueResult();
	}

	@Override
	public void saveOrUpdate(Role role) {
		sessionFactory.getCurrentSession().saveOrUpdate(role);
	}

	@Override
	public void deleteById(long roleId) {
		sessionFactory.getCurrentSession().delete(retrieveById(roleId));
	}
}
