/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012 ..
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.sp.dao.hibernate;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.myvouchee.sp.dao.UserDao;
import com.myvouchee.sp.domain.User;


/**
 * Hibernate User DAO implementation. 
 * 
 * @author Jitendra Chahan
 */
@Repository
public class HibernateUserDao implements UserDao {

	private SessionFactory sessionFactory;

	@Autowired
	public HibernateUserDao(@Qualifier("sp") SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<User> retrieveAll() {
		return sessionFactory.getCurrentSession().createQuery("from User user order by user.name")
				.list();
	}

	@Override
	public User retrieveById(long id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	@Override
	public User retrieveByName(String name) {
		return (User) sessionFactory.getCurrentSession()
				.createQuery("from User user where user.name = :name").setString("name", name)
				.uniqueResult();
	}

	@Override
	public void saveOrUpdate(User user) {
		if (user.getId() != null) {
			sessionFactory.getCurrentSession().merge(user);
		} else {
			sessionFactory.getCurrentSession().saveOrUpdate(user);
		}
	}

	@Override
	public void deleteById(long id) {
		sessionFactory.getCurrentSession().delete(retrieveById(id));
	}
}
