/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012 Iviz Technosolutions Pvt. Ltd.
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.sp.dao;

import java.util.List;

import com.myvouchee.sp.domain.Role;


/**
 * Basic DAO class for the Role entity.
 * 
 * @author dshannon
 */
public interface RoleDao {

	/**
	 * @return
	 */
	List<Role> retrieveAll();

	/**
	 * @param id
	 * @return
	 */
	Role retrieveById(long id);

	/**
	 * @param name
	 * @return
	 */
	Role retrieveByName(String name);

	/**
	 * @param role
	 */
	void saveOrUpdate(Role role);

	/**
	 * @param roleId
	 */
	void deleteById(long roleId);

}
