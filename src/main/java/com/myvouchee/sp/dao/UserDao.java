/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.sp.dao;

import java.util.List;

import com.myvouchee.sp.domain.User;


/**
 * Basic DAO class for the User entity.
 * 
 * @author dshannon
 */
public interface UserDao {

	/**
	 * @return
	 */
	List<User> retrieveAll();

	/**
	 * @param id
	 * @return
	 */
	User retrieveById(long id);

	/**
	 * @param name
	 * @return
	 */
	User retrieveByName(String name);

	/**
	 * @param user
	 */
	void saveOrUpdate(User user);

	/**
	 * @param id
	 */
	void deleteById(long id);
}
