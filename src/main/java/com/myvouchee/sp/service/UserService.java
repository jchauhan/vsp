/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012 Iviz Technosolutions Pvt. Ltd.
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.sp.service;

import java.util.List;

import com.myvouchee.sp.domain.Role;
import com.myvouchee.sp.domain.User;


/**
 * @author Jitendra Chauhan
 * 
 */
public interface UserService {

	/**
	 * @return
	 */
	List<User> loadAllUsers();

	/**
	 * @param userId
	 * @return
	 */
	User loadUser(long userId);

	/**
	 * @param name
	 * @return
	 */
	User loadUser(String name);

	/**
	 * @param user
	 */
	void updateUser(User user);

	/**
	 * @param userId
	 */
	void deleteById(int userId);

	/**
	 * @param user
	 */
	void createUser(User user);

	/**
	 * @return
	 */
	List<Role> loadAllRoles();

	/**
	 * @param roleId
	 * @return
	 */
	Role loadRole(int roleId);

	/**
	 * @param name
	 * @return
	 */
	Role loadRole(String name);

	/**
	 * @param role
	 */
	void storeRole(Role role);

	/**
	 * @param roleId
	 */
	void deleteRole(long roleId);

	/*
	 * @param FbUserId
	 */
	User storeOrUpdateFbUser(long fbUserId) throws Exception;

	User storeOrUpdateFbUser(long fbUserId, String fbJson) throws Exception;
}
