/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012 ...
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/

package com.myvouchee.sp.service;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Custom password encoder implementation of @see PasswordEncoder
 * 
 * @author Jitendra Chauhan
 * @see PasswordEncoder
 */
@Service
public class VcsPasswordEncoder implements PasswordEncoder {

	@Override
	public String encodePassword(String rawPass, Object salt) throws DataAccessException {
		String encodedPass = null;

		try {
			if (salt == null) {
				encodedPass = generatePasswordHash(rawPass, generateSalt());
			} else {
				encodedPass = generatePasswordHash(rawPass, (String) salt);
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return encodedPass;
	}

	@Override
	public boolean isPasswordValid(String encPass, String rawPass, Object salt)
			throws DataAccessException {

		return encPass.equals(encodePassword(rawPass, salt));
	}

	/**
	 * @param bytes
	 * @return
	 */
	public String convertBytesToHexString(byte[] bytes) {
		StringBuffer hexString = new StringBuffer();

		for (int i = 0; i < bytes.length; i++) {
			String temp = Integer.toHexString(0xFF & bytes[i]);
			if (temp.length() == 1) {
				hexString.append('0');
			}
			hexString.append(temp);
		}

		return hexString.toString();
	}

	/**
	 * @return
	 */
	public String generateSalt() {
		java.util.UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

	/**
	 * @param password
	 * @param salt
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public String generatePasswordHash(String password, String salt)
			throws NoSuchAlgorithmException {
		SimpleHash hash = new SimpleHash("SHA-256", password, salt, 1);

		//TODO using standard hashing algo by shiro
//		String newPassword = password + salt;
//		MessageDigest msgDigest = MessageDigest.getInstance("SHA-256");
//		msgDigest.update(newPassword.getBytes());
//		String pwHash = convertBytesToHexString(msgDigest.digest());
		
		String pwHash = convertBytesToHexString(hash.getBytes());
		return pwHash;
	}

}