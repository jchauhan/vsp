/*******************************************************************************
 * ////////////////////////////////////////////////////////////////////////
 * //
 * //     Copyright (c) 2006-2012 ...
 * ////////////////////////////////////////////////////////////////////////
 ******************************************************************************/
package com.myvouchee.sp.service;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myvouchee.sp.dao.RoleDao;
import com.myvouchee.sp.dao.UserDao;
import com.myvouchee.sp.domain.Role;
import com.myvouchee.sp.domain.User;
import com.myvouchee.vcs.dao.FbRemoteUserDao;
import com.myvouchee.vcs.dao.FbUserDao;
import com.myvouchee.vcs.domain.FbUser;


@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

	private UserDao userDao = null;
	private RoleDao roleDao = null;
	private FbUserDao fbUserDao = null;
	private FbRemoteUserDao fbRemoteUserDao = null;
	
	private VcsPasswordEncoder encoder = new VcsPasswordEncoder();

	@Autowired
	public UserServiceImpl(UserDao userDao, RoleDao roleDao, 
			FbUserDao fbUserDao, FbRemoteUserDao fbRemoteUserDao) {
		this.userDao = userDao;
		this.roleDao = roleDao;
		this.fbUserDao = fbUserDao;
		this.fbRemoteUserDao = fbRemoteUserDao;
	}

	@Override
	public List<User> loadAllUsers() {
		return userDao.retrieveAll();
	}

	@Override
	public User loadUser(long userId) {
		return userDao.retrieveById(userId);
	}

	@Override
	public User loadUser(String name) {
		return userDao.retrieveByName(name);
	}

	@Override
	@Transactional(readOnly = false)
	public void updateUser(User user) {
		if ((user.getUnencryptedPassword() != null) && (user.getUnencryptedPassword().length() > 0)) {
			encryptPassword(user);
		}
		userDao.saveOrUpdate(user);
	}

	@Override
	@Transactional(readOnly = false)
	public void deleteById(int userId) {
		userDao.deleteById(userId);
	}

	@Override
	@Transactional(readOnly = false)
	public void createUser(User user) {
		encryptPassword(user);
		//FIX me need to remove following hardcoding
		Role role = loadRole("END_USER");
		user.setRole(role);
		userDao.saveOrUpdate(user);
	}

	@Override
	public List<Role> loadAllRoles() {
		return roleDao.retrieveAll();
	}

	@Override
	public Role loadRole(int roleId) {
		return roleDao.retrieveById(roleId);
	}

	@Override
	public Role loadRole(String name) {
		return roleDao.retrieveByName(name);
	}

	@Override
	@Transactional(readOnly = false)
	public void storeRole(Role role) {
		roleDao.saveOrUpdate(role);
	}

	@Override
	@Transactional(readOnly = false)
	public void deleteRole(long roleId) {
		roleDao.deleteById(roleId);
	}
	
	@Override
	@Transactional(readOnly = false)
	public User storeOrUpdateFbUser(long fbUserId) throws Exception {
		FbUser fbUser = fbUserDao.retrieveByFbUserId(fbUserId);
		User user = null;
		//If the user is already registered
		if(fbUser != null)
		{
			long userId = fbUser.getUserId();
			user = userDao.retrieveById(userId);
		}
		else
		{
			fbUser = fbRemoteUserDao.retrieveById(fbUserId);
			createUser(fbUser);
		}
		return user;
	}
	
	@Override
	@Transactional(readOnly = false)
	public User storeOrUpdateFbUser(long fbUserId, String fbJson) throws Exception {
		FbUser fbUser = fbUserDao.retrieveByFbUserId(fbUserId);
		User user = null;
		//If the user is already registered
		if(fbUser != null)
		{
			long userId = fbUser.getUserId();
			user = userDao.retrieveById(userId);
		}
		else
		{
			fbUser = fbRemoteUserDao.parseFbUserResponse(fbJson);
			createUser(fbUser);
		}
		return user;
	}
	
	private void createUser(FbUser fbUser)
	{
		User user = new User();
		user.setEmail(fbUser.getEmail());
		user.setUsername(fbUser.getUsername());
		user.setName(fbUser.getName());
		user.setGender(fbUser.getGender());
		user.setActive(true);
		user.setVerified(true);
		user.setUnencryptedPassword(UUID.randomUUID().toString());
		createUser(user);
		fbUser.setUserId(user.getId());
		fbUserDao.saveOrUpdate(fbUser);
	}
	
	
	private void encryptPassword(User user) {
		try {
			user.setSalt(encoder.generateSalt());
			user.setPassword(encoder.generatePasswordHash(user.getUnencryptedPassword(),
					user.getSalt()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
}
