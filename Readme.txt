It is security platform to performing following
1. Authentication
 a. Form Based
 b. Facebbook
 c. <<you can add others>>
2. Authorization 
 a. Role Based


How to Run:
============

** Create Database named 'mv_sp' on Mysql database

** Configure a database resource on tomcat server
 Add following context on inside $TOMCAT_HOME/conf/context.xml inside <Context> tag. 


     <Resource name="jdbc/myvouchee/sp" auth="Container" type="com.mchange.v2.c3p0.ComboPooledDataSource"
                idleConnectionTestPeriod="3600" maxPoolSize="30" minPoolSize="5"
               user="<db-username>" password="<db-password>" driverClass="com.mysql.jdbc.Driver"
               factory="org.apache.naming.factory.BeanFactory"
               jdbcUrl="jdbc:mysql://localhost:3306/mv_sp"/>


** Package to war 
 Run 'mvn clean package'


** Copy the target/sp.war file to $TOMCAT_HOME/webapps directory

** Start the tomcat server (if not already started, no need to restart it otherwise)

